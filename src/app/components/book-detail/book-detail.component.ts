import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Book from 'src/app/models/book.model';
import { UserName } from 'src/app/models/user.model';
import { BookService } from 'src/app/services/book.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private readonly bookService:BookService, private readonly userService:UserService) { }

  bookId: string | null = 'default'
  book: Book = {
    bookId: 0,
    title: "string",
    genre: "string",
    edition: "string",
    retailPrice: 0,
    publisherId: "string",
    coverImg: "string"
  }

  ngOnInit(): void {
    this.bookId = this.route.snapshot.paramMap.get('id')
    this.bookService.getBook(this.bookId)
    .subscribe(
    (book:Book)=>{
      this.book = book
    },
    (error :HttpErrorResponse)=>{
      console.log(error)
    })

  }

  get user():UserName|undefined{
    return this.userService.user()
}

}
