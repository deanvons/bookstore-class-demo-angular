import { Component, OnInit } from "@angular/core";
import { UserName } from "src/app/models/user.model";
import { UserService } from "src/app/services/user.service";

@Component({
    selector: 'app-header',
    templateUrl:'./app-header.component.html'
})
export class AppHeader implements OnInit{

    constructor (private readonly userService:UserService)  {}

    ngOnInit(){
        this.userService.getUser()
    }

    changeUser(){
        //call the setter
        let updatedUser:UserName = {
            name:{
                title: "Mr",
                first: "Nicholas",
                last: "Mengucci"
            },
            picture:{
                large:"https://randomuser.me/api/portraits/men/10.jpg",
                medium:"https://randomuser.me/api/portraits/med/men/10.jpg",
                thumbnail:"https://randomuser.me/api/portraits/thumb/men/10.jpg"
            }
        }

        this.user = updatedUser

    }

get user():UserName|undefined{
    return this.userService.user()
}

set user(user:UserName|undefined){
    this.userService.setUser(user)
}


}