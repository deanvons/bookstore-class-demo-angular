import { Component, Input, Output, EventEmitter } from "@angular/core";
import Book from "src/app/models/book.model";

@Component({
    selector: 'app-book-item',
    templateUrl: './book-item.component.html'
})
export class BookItem {
    @Input() book?: Book

    @Output() bookDetailsRequested: EventEmitter<any> = new EventEmitter()

    bookClicked() {
        //
        this.bookDetailsRequested.emit(this.book?.bookId)
    }
}
