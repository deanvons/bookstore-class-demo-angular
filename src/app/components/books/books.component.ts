import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import Book from "src/app/models/book.model";
import { Router, RouterLink } from '@angular/router'
import { BookService } from "src/app/services/book.service";
import {map, filter } from "rxjs/operators";

@Component({
    selector: 'app-books',
    templateUrl: './books.component.html',
    styleUrls: []
})
export class Books implements OnInit {
    public books: Book[] = []

    constructor(private readonly router: Router, private readonly bookService:BookService) {

    }

    ngOnInit(): void {
        //get data here
        this.bookService.getAllBooks()
        .pipe(
            map(books=> books.filter(book=> book.genre === "Sport Science"))
        )
            .subscribe(
                (books: Book[]) => {
                    this.books = books
                },
                (error: HttpErrorResponse) => {
                    console.log(error)
                }
            )
    }

   

    showBookDetails(bookId: number) {
        this.router.navigateByUrl(`details/${bookId}`)
    }
}