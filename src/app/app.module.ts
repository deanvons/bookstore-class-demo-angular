import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { BookItem } from './components/book-item/book-item.component';
import { Books } from './components/books/books.component';
import { BookDetailComponent } from './components/book-detail/book-detail.component';
import { AppRoutingModule } from './app-routing.module';
import { AppHeader } from './components/app-header/app-header.component';

@NgModule({
  declarations: [
    AppComponent,
    Books,
    BookItem,
    BookDetailComponent,
    AppHeader
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
