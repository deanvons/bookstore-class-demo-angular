import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BookDetailComponent } from "./components/book-detail/book-detail.component";
import { Books } from "./components/books/books.component";

const routes: Routes = [{
    path: '',
    pathMatch: 'full',
    component: Books

},
{
    path: 'details/:id',
    component: BookDetailComponent
}
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }