export interface UserName
    {
        name:Name
        picture:Picture
    }

export interface Picture{

        large:string,
        medium:string,
        thumbnail:string
}

export interface Name{
    title:string,
    first:string,
    last:string
}