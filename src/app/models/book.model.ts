export default interface Book {
    bookId: number
    title: string
    genre: string
    edition: string
    retailPrice: number
    coverImg: string
    publisherId: string
}