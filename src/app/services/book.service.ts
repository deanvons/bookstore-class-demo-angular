import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import  Book  from 'src/app/models/book.model'

@Injectable({
    providedIn:'root'
})
export class BookService{

    constructor(private readonly http:HttpClient){

    }

getAllBooks(){
//fetch book array from api
 return this.http.get<Book[]>('https://localhost:44304/api/Books')
}

getBook(id:string | null){
    //fetch book by id from api
     return this.http.get<Book>(`https://localhost:44304/api/Books/${id}`)
    }


}