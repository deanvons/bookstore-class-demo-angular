import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Name, Picture, UserName } from "../models/user.model";
import {finalize, map,tap} from 'rxjs/operators'

@Injectable({
    providedIn:'root'
})
export class UserService{

    private _user:UserName | undefined

    constructor (private readonly http:HttpClient){

    }

    getUser():void{

        this.http.get<any>('https://randomuser.me/api/?seed=7e5eba56969b6646')
        .pipe(
            tap(originalData => console.log(originalData)),
            finalize(()=>console.log("Subscription DonE!!")),
            map(user =>{
                let tempName:Name = user.results[0].name
                let tempPic:Picture = user.results[0].picture
                let tempUsername:UserName = {
                    name:tempName,
                    picture:tempPic
                }
                return tempUsername
            })
        )
        .subscribe((user:UserName)=>{
            
            console.log(user)

            this._user = user

        },
        (error:HttpErrorResponse)=>{
            console.log(error)
        })

    }

    public user():UserName|undefined{
        return this._user
    }

    public setUser(val:UserName|undefined){
        this._user = val
    }

}