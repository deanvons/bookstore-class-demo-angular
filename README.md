[x] - Book model
[x] - Book item component
[x] - Http request
[x] - Pass props to Book item
[x] - Routing
[x] - Event emitter
[x] - Move Book fetch logic to service
[x] - Service for Book details fetch
[x] - User state with state (service)
[x] - User service state getters and setters
[x] - RxJS mapping added to user service
[x] - RxJS map filter example added to books component